#include <iostream>
#include <wiringPi.h>

#define PWM_PIN 1

using namespace std;

int main(void){
  wiringPiSetup();

  pinMode(PWM_PIN, PWM_OUTPUT);
  pwmSetMode(PWM_MODE_MS);
  pwmSetRange(1024);
  pwmSetClock(38);
  //output f(Hz) = 19.2*10^6/range/clock
  //output = 19.2*10^6/1024/38 approx = 493Hz
  pwmWrite(PWM_PIN, 0);

  while(true){
    for(int i = 0; i < 1024; i += 32){
      pwmWrite(PWM_PIN, i);
      cout<<"Setting pwm to: "<<i<<endl;
      delay(250);
    }

    for (int i = 1024; i > 0; i -= 32){
      pwmWrite(PWM_PIN, i);
      cout<<"Setting pwm to: "<<i<<endl;
      delay(250);
    }
  }
}
