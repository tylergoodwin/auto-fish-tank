#include <iostream>
#include <wiringPi.h>

#define LDR_2 29
#define LDR_1 28
#define LDR_0 27

using namespace std;

int main(void){
  wiringPiSetup();

  pinMode(LDR_2, INPUT);
  pinMode(LDR_1, INPUT);
  pinMode(LDR_0, INPUT);
  int level;

  while(true){
   level = 0;
   if(digitalRead(LDR_2) == LOW)
      level += 4;
   if(digitalRead(LDR_1) == LOW)
      level += 2;
   if(digitalRead(LDR_0) == LOW)
      ++level;
   cout<<"Level:"<<level<<endl;
   delay(250);
  }
}
