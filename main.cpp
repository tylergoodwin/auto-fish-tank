#include "main.h"

int main(int argc, char *argv[]){

  auto app = Gtk::Application::create(argc, argv, "rmit.design3b.team17.autofishtank");

  FishWindow fish_window;

  fish_window.set_default_size(WINDOW_WIDTH, WINDOW_HEIGHT);

  return app->run(fish_window);
}