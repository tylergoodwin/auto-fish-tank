#include "fishtank.h"
#include <iostream>


using namespace std;

#define TEMP_PIN 7
#define WATER_EN 24
#define WATER_PIN 25
#define LIGHT_PIN 1

#define FEED_PIN 26

#define LDR_0 29
#define LDR_1 28
#define LDR_2 27

FishTank::FishTank(){
  light_level = 5;
  light = false;
  pwm_level = 256;
  list_sum = 0;

  wiringPiSetup();

  //set up pins
  pinMode(LIGHT_PIN, PWM_OUTPUT);
  pwmSetMode(PWM_MODE_MS);
  pwmSetRange(1024);
  pwmSetClock(38);

  pinMode(WATER_EN, OUTPUT);
  pinMode(WATER_PIN, INPUT);

  pinMode(FEED_PIN, OUTPUT);
  digitalWrite(FEED_PIN, LOW);

  pinMode(LDR_2, INPUT);
  pinMode(LDR_1, INPUT);
  pinMode(LDR_0, INPUT);

  pwmWrite(LIGHT_PIN, 0);
}

FishTank::~FishTank(){

}


float FishTank::getTemperature(){
  float temp;
  int i, j;
  int fd;
  int ret;
  char buf[BUFSIZE];
  char tempBuf[5];

  fd = open("/sys/bus/w1/devices/28-000007c74ca2/w1_slave", O_RDONLY);
  if(-1 == fd){
  perror("open device file error");
  return -1;
  }
  while(1){
    ret = read(fd, buf, BUFSIZE);
    if(0 == ret){
    break; 
    }
    if(-1 == ret){
      if(errno == EINTR){
        continue; 
      }
    perror("read()");
    close(fd);
    return -1;
    }
  }
  for(i=0;i<sizeof(buf);i++){
    if(buf[i] == 't'){
      for(j=0;j<sizeof(tempBuf);j++){
        tempBuf[j] = buf[i+2+j]; 
      }
    } 
  }
  temp = (float)atoi(tempBuf) / 1000;
  close(fd);
  return temp;
}

bool FishTank::getWaterLevel(){
  //set enable high
  digitalWrite(WATER_EN, HIGH);
  delay(250);
  //read water level
  int ret = digitalRead(WATER_PIN);

  if(ret == HIGH){
    return true;
  }
  return false;
}

void FishTank::feed(int amount){
  digitalWrite(FEED_PIN, HIGH);
  delay(amount*500);
  digitalWrite(FEED_PIN, LOW);
}

void FishTank::setLightLevel(int level){
  light_level = level;
}

int FishTank::getLightLevel(){
  return light_level;
}

int FishTank::getLDRLevel(){
  int level = 0;
   if(digitalRead(LDR_2) == LOW)
      level += 4;
   if(digitalRead(LDR_1) == LOW)
      level += 2;
   if(digitalRead(LDR_0) == LOW)
      ++level;
  return level;
}

void FishTank::toggleLight(){
  if(light)
    light = false;
  else
    light = true;
}

void FishTank::updateLightLevel(){
  int ldr = getLDRLevel();
  int avg;
  level_list.push_back(ldr);
  list_sum += ldr;

  if(level_list.size() > 30){
    int front = level_list.front();
    list_sum -= front;
    level_list.pop_front();
  }

  avg = list_sum/level_list.size();

  int old_pwm = pwm_level;
  if(light){
    if(light_level == 8){
      pwm_level = 1024;
    }
    else if(avg < light_level){
      if(pwm_level < 1024)
        pwm_level += 1;
    }
    else //if(pwm_level > light_level){
    {  if(pwm_level > 0)
        pwm_level -= 1;
    }
    /*else {
      --pwm_level;
    }*/

    if(pwm_level != old_pwm)
      pwmWrite(LIGHT_PIN, pwm_level);
  }
  else {
    pwmWrite(LIGHT_PIN, 0);
  }
}
