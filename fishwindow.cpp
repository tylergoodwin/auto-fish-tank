#include "fishwindow.h"
#include <iostream>
#include <iomanip>
#include <sstream>

using namespace std;

FishWindow::FishWindow()
: feed_btn("Feed Now"),
  light_btn("Toggle Light"),
  lightup_btn("Up"),
  lightdown_btn("Down"),
  title("Your Fish Tank"),
  lightlevel_lbl("5"),
  temp_lbl("Temperature:"),
  waterlevel_lbl("Water Level: Checking..."),
  light_lbl("Brightness")
{
  this.set_cursor(Cursor(Gdk::ARROW));
  add(grid);

  grid.set_row_homogeneous(true);
  grid.set_column_homogeneous(true);

  title.set_size_request(310, 10);

  grid.attach(title,0,0,2,1);

  //left column
  grid.attach_next_to(temp_lbl, title, Gtk::POS_BOTTOM, 1,1);
  grid.attach_next_to(waterlevel_lbl, temp_lbl, Gtk::POS_BOTTOM, 1, 1);
  grid.attach_next_to(feed_btn, waterlevel_lbl, Gtk::POS_BOTTOM, 1, 1);
  grid.attach_next_to(light_btn, feed_btn, Gtk::POS_BOTTOM, 1, 1);

  //right column
  grid.attach_next_to(light_lbl, temp_lbl, Gtk::POS_RIGHT, 1, 1);
  grid.attach_next_to(lightup_btn, light_lbl, Gtk::POS_BOTTOM, 1, 1);
  grid.attach_next_to(lightlevel_lbl, feed_btn, Gtk::POS_RIGHT, 1, 1);
  grid.attach_next_to(lightdown_btn, light_btn, Gtk::POS_RIGHT, 1, 1);

  grid.set_size_request(320, 240);
  
  //button handlers
  feed_btn.signal_clicked().connect(sigc::mem_fun(*this, &FishWindow::on_button_feed));

  lightup_btn.signal_clicked().connect(sigc::mem_fun(*this, &FishWindow::on_button_up));

  lightdown_btn.signal_clicked().connect(sigc::mem_fun(*this, &FishWindow::on_button_down));

  light_btn.signal_toggled().connect(sigc::mem_fun(*this, &FishWindow::on_light_toggle));

  Glib::signal_timeout().connect(sigc::mem_fun(*this, &FishWindow::updateTempurature),1000, 15);

  Glib::signal_timeout().connect(sigc::mem_fun(*this, &FishWindow::updateWaterLevel),10000, 10);

  Glib::signal_timeout().connect(sigc::mem_fun(*this, &FishWindow::updateLightLevel),100, 10);

  show_all_children();
}

FishWindow::~FishWindow(){

}

/*------------------
  Signal Handlers
-------------------*/


bool FishWindow::updateTempurature(){

  float temp = fishtank.getTemperature();
  Glib::ustring s = "Temperature: ";
  
  if(temp == -1){//check for error
    s += " Error";
  }
  else {
    std::stringstream ss;
    ss << std::fixed << std::setprecision(2) << temp;
    s += ss.str();   
  }
  //update display
  temp_lbl.set_text(s);
  return true;
}
bool FishWindow::updateWaterLevel(){
  if(fishtank.getWaterLevel())
    waterlevel_lbl.set_text("Water Level: Good");
  else
    waterlevel_lbl.set_text("Water Level: Low");
  return true;
}
void FishWindow::on_button_feed(){
  cout<<"Feeding Fish"<<endl;
  fishtank.feed(1);
}
void FishWindow::on_button_up(){
  if(lightup_btn.get_sensitive()){
    int level = fishtank.getLightLevel();

    if(level == MIN_LIGHT_LEVEL){
      lightdown_btn.set_sensitive(true);
    }

    lightlevel_lbl.set_text(std::to_string(++level));

    fishtank.setLightLevel(level);
    
    if(level == MAX_LIGHT_LEVEL){
      lightup_btn.set_sensitive(false);
    }
  }

}
void FishWindow::on_button_down(){
  if(lightdown_btn.get_sensitive()){
    int level = fishtank.getLightLevel();

    if(level == MAX_LIGHT_LEVEL){
      lightup_btn.set_sensitive(true);
    }
    
    lightlevel_lbl.set_text(std::to_string(--level));

    fishtank.setLightLevel(level);

    if(level == MIN_LIGHT_LEVEL){
      lightdown_btn.set_sensitive(false);
    }
  }
}
void FishWindow::on_light_toggle(){
  fishtank.toggleLight();
}

bool FishWindow::updateLightLevel(){
  fishtank.updateLightLevel();

  Glib::ustring s = "Level: " + std::to_string(fishtank.getLightLevel()); // + "Ldr: " + std::to_string(fishtank.getLDRLevel());
  lightlevel_lbl.set_text(s);

  return true;
}