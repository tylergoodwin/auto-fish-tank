#ifndef FISH_TANK_APP_FISHTANK_H
#define FISH_TANK_APP_FISHTANK_H

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <wiringPi.h>
#include <list>

#define BUFSIZE 128

//These values arethe wiringPi pin numbers, not the
//physical pin numbers. 
#define TEMP_PIN 7
#define WATER_EN 24
#define WATER_PIN 25
#define LIGHT_PIN 1

#define LDR_0 29
#define LDR_1 28
#define LDR_2 27

//gpio18 for turning backlight on/off
//   Might need to do some dodgy stuff to utlize another pin
//   because that pin is used for PWM, which is used for the lights

class FishTank{

public:
  FishTank();
  ~FishTank();
  float getTemperature();
  bool getWaterLevel();
  void feed(int amount);
  void setLightLevel(int level);
  int getLightLevel();
  int getLDRLevel();
  void toggleLight();
  void updateLightLevel();

private:
  int light_level;
  bool light;
  int pwm_level;

  std::list<int> level_list;
  int list_sum;

};

#endif